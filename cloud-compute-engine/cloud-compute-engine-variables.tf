## Ceate VM App Server
## Put Information
## Site reference https://www.terraform.io/docs/providers/google/r/compute_instance.html#machine_type
variable "zone" {
        default = "asia-southeast1-b"
    }
variable "vm_count" {
        default = "1"
    }
variable "vm_name" {
        default = " "
    }
variable "vm_type" {
        default = "custom-1-1024"
    }
variable "vm_image" {
        default = "centos-7"
    }
    //reference https://cloud.google.com/compute/docs/images
variable "vm_adddisk_type" {
        default = "pd-standard"
    }
variable "vm_adddisk_size" {
        default = "50"
    }
variable "vm_network" {
        default = "default"
    }
variable "vm_subnetwork" {
        default = "default"
    }

variable "vm_tag1" {
        default = "http-server"
    }
variable "vm_tag2" {
        default = "https-server"
    }
variable "vm_tag3" {
        default = "dmp-common"
    }

//Key Name
variable "pub_rsa" {
        default = "name"
    }

//Key Path
variable "pub_path" {
        default = "c/rsa.text"
    }

## End

