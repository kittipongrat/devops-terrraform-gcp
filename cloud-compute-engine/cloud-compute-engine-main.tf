
resource "google_compute_disk" "server" {
    count       = "${var.vm_count}"
    name        = "${var.vm_name}-00${count.index}-disk01"
    type        = "${var.vm_adddisk_type}"  
    size        = "${var.vm_adddisk_size}" 
} 
resource "google_compute_instance" "server" {    
  zone        = "${var.zone}"
  count       = "${var.vm_count}"
  name         = "${var.vm_name}-00${count.index}"
  machine_type = "${var.vm_type}"
  boot_disk {
    initialize_params {
      image     =  "${var.vm_image}"
    }
  }
  attached_disk {
    source = "${var.vm_name}-00${count.index}-disk01"
  }

 metadata = {

    sshkeys = "${var.pub_rsa}:${file("${var.pub_path}")}"
   
  }


  network_interface {
    network       = "${var.vm_network}"
    subnetwork    = "${var.vm_subnetwork}"
  }
  
  lifecycle {
    ignore_changes = ["attached_disk"]
  }      
  
  tags = ["${var.vm_tag1}" ,"${var.vm_tag2}" ,"${var.vm_tag3}"]

  allow_stopping_for_update = true
}

