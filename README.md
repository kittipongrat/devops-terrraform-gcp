# Module reference examples  
  
* cloud-build-trigger  
    ```  
    module "cloud-build-devops-int-pipeline" {  
        source = "bitbucket.org/kittipongrat/devops-terrraform-gcp.git//cloud-build-trigger?ref=V2.1.0"  
        repository_name = "bitbucket_seubpong_devops-utils-app"  
    }  
    ```  
  
* cloud-pubsub-topic  
    ```  
    module "pubsub-deployment-to-gcp-topic" {  
        source = "bitbucket.org/kittipongrat/devops-terrraform-gcp.git//cloud-pubsub-topic?ref=V2.1.0"  
        topic_name = "deployment-to-gcp-topic"  
    }  
    ```  
* cloud-pubsub-subscription-pull  
    ```  
    module "pubsub-deployment-to-gcp-subscription" {  
        source = "bitbucket.org/kittipongrat/devops-terrraform-gcp.git//cloud-pubsub-subscription-pull?ref=V2.1.0"  
        topic_name = "deployment-to-gcp-topic"  
        subscription_name = "deployment-to-gcp-subscription"  
    }   
    ```

# Instruction to create the release branch
* Run the following command to create a release branch where x,y and z are integers.
    ```
    ./branch_release.pl -rv x.y.z
    ```