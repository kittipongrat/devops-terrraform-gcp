resource "google_pubsub_subscription" "generic-subscription-pubsub" {
    name = "${var.subscription_name}"
    topic = "${var.topic_name}"

    # 20 minutes
    message_retention_duration = "1200s"
    retain_acked_messages = true

    ack_deadline_seconds = 20

    expiration_policy {
        ttl = "300000.5s"
    }
}