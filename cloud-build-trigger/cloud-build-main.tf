resource "google_cloudbuild_trigger" "multibranch-trigger" {
  trigger_template {
    branch_name = "development|develop|feature-*|master"
    
    #Repository name in the Google Source Repository
    #Google Source Repository need to be synced to Git external repo manually
    repo_name   = "${var.repository_name}"
  }

  filename = "${var.pipeline_def_file}"
}
